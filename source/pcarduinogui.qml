import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.1
import QtCharts 2.3
import QtQuick.Controls.Material 2.0
import QtQuick.Dialogs 1.1




ApplicationWindow {
    id: applicationWindow
    title: qsTr("PCArduino Data Logger")
    width: 800
    height: 600
    minimumWidth: 800
    minimumHeight: 600
    visible: true


    Component.onCompleted: {

        setX(Screen.width / 2 - width / 2);
        setY(Screen.height / 2 - height / 2);
    }
    Material.accent: Material.Cyan

    menuBar: MenuBar {
        id: menuBar1

        Menu {
            title: qsTr("&File")



            MenuItem {
                text : qsTr("Clear")
                onTriggered: py_MainApp.effacer(lineSeries, lineSeries2,lineSeries3)
            }


            MenuItem {
                text : qsTr("Quit")
                onTriggered: {
                    Qt.quit()
                }
            }

        }
        Menu {
            title: qsTr("&Data")


            MenuItem {
                text: qsTr("Send to notepad")
                onTriggered: py_MainApp.notepad()
            }





        }
        Menu {
                title: qsTr("&Settings")


           /* Menu {
                title: qsTr("Language")

                MenuItem {
                text: qsTr("Francais")
                icon.source: "icon_fr.jpg"
                icon.color: "transparent"


                onTriggered: py_MainApp.traductionFr()
                }

                MenuItem {
                text: qsTr("English")
                icon.source: "icon_en.jpg"
                icon.color: "transparent"



                onTriggered: py_MainApp.traductionEn()
                }

            }*/

            MenuItem {
                text: qsTr("About")
                onTriggered: messageDialog.open()

        }





    }
    }
    MessageDialog {
        id: messageDialog
        visible: false
        title: qsTr("About")
        text: "PCArduino Data Logger
V1.0"
        Material.accent: Material.Cyan
        onAccepted: {

            messageDialog.close()
        }

    }

    ChartView {
        id: line
        visible: true
        legend.visible: false
        anchors.topMargin: 21
        anchors.left: parent.left
        anchors.leftMargin: 402
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top

        LineSeries{
            id: lineSeries
            name: "Arduino"
            useOpenGL: true

            axisX: ValueAxis {
                id: axisX
                titleText: "Time (s)"
                min: 0
                max: 10.0
            }
            axisY: ValueAxis {
                id: axisY
                min: 0
                max: 500
            }
            onPointAdded:{ axisX.max = xaxismax
                axisY.max = yaxismax
                axisY.min = yaxismin
                etatAcquisition.text = etatacquisition
            }

        }
        LineSeries{
            id: lineSeries2

            useOpenGL: true
            axisX: axisX

        }
        LineSeries{
            id: lineSeries3

            useOpenGL: true
            axisX: axisX

        }




    }

    GroupBox {
        id: acquisition
        x: 12
        width: 384
        height: 362
        z: 1
        anchors.top: parent.top
        anchors.topMargin: 178
        title: qsTr("")

        Button {
            y: 169
            width: 197
            height: 48
            text: qsTr("Start ")
            id : btnstart

            objectName: "startButton"
            z: 1
            focusPolicy: Qt.TabFocus
            anchors.left: parent.left
            anchors.leftMargin: 82
            Layout.fillWidth: false
            Layout.fillHeight: false
            Layout.columnSpan: 2
            Material.background: Material.Cyan
            onClicked: {
                delaySwitch.start()
                btnstart.enabled = false
                etatAcquisition.text= "Wait..."
                py_MainApp.update(lineSeries ,lineSeries2, lineSeries3,axisX ,nombreDePoints.text, tempsDechantillonge.text)

            }
        }
        Timer {
            id: delaySwitch
            interval: (parseInt(nombreDePoints.text) * parseInt(tempsDechantillonge.text))+7000
            running: true;
            repeat: false
            onTriggered: {
                btnstart.enabled = true
            }
        }
        Label {
            x: 11
            y: 70
            width: 124
            height: 40
            text: qsTr("Sampling time (ms) :")
        }

        TextField {
            id: nombreDePoints
            x: 160
            y: 10
            width: 200
            height: 40
            text: "120"
            focus: true
            Layout.fillWidth: true
            validator: IntValidator {bottom: 1; top: 999;}
            onEditingFinished:
            {
                totalResult.text= parseInt(nombreDePoints.text) * parseInt(tempsDechantillonge.text)/1000
              }


        }

        Label {
            x: 11
            y: 24
            width: 124
            height: 40
            text: qsTr("Number of points :")
        }

        TextField {
            id: tempsDechantillonge
            x: 161
            y: 56
            width: 199
            height: 40
            text: "50"
            Layout.fillWidth: true

            onEditingFinished:
            {
                totalResult.text= parseInt(nombreDePoints.text) * parseInt(tempsDechantillonge.text) /1000

            }
        }

        Label {
            id: etatAcquisition
            x: 149
            y: 303
            width: 140
            height: 40
            text: qsTr("Stop")
        }

        Button {
            id: button
            y: 233
            width: 196
            height: 48
            text: qsTr("Stop")

            Material.background: "#EF9A9A"

            anchors.left: parent.left
            anchors.leftMargin: 83
            onClicked: {

                delaySwitch.stop()
                btnstart.enabled = true
                py_MainApp.stopThread()
                etatAcquisition.text = "Stop"

            }
        }

        Label {
            id: label
            x: 3
            y: 303
            width: 140
            height: 40
            text: qsTr("State of acquisition:")
        }

        TextArea {
            id: totalResult
            x: 159
            y: 109
            width: 199
            height: 39
            text: "6"
            readOnly: true
            background: Material.Grey
        }

        Label {
            x: 11
            y: 116
            width: 124
            height: 40
            text: "Total (s) :"
        }






    }

    GroupBox {
        id: configArduino
        x: 12
        width: 384
        height: 89
        z: 2
        anchors.top: parent.top
        anchors.topMargin: 83
        title: qsTr("")

        Button {
            x: 193
            width: 167
            text: qsTr("Send to arduino")
            id: btnConfig
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            anchors.right: parent.right
            anchors.rightMargin: 0
            Layout.fillWidth: false
            Layout.fillHeight: false
            Layout.columnSpan: 2
            onClicked: {

                py_MainApp.configArduino(comboBox.currentText)



            }
        }





    Timer {
        id: delayConfig
        interval: 16000
        running: true;
        repeat: false
        onTriggered: {
            btnConfig.enabled = true
            btnstart.enabled = true
        }
    }

        ComboBox {
            id: comboBox
            width: 176
            anchors.top: parent.top
            anchors.topMargin: 0
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            anchors.left: parent.left
            anchors.leftMargin: 0
            model: ListModel {
                id: model
                ListElement { text: qsTr("Analog A0" )}
                ListElement { text: qsTr("Analog A1" )}
                ListElement { text: qsTr("Accelerometer") }


            }


        }






    }

    GroupBox {
        id: groupBox
        x: 12
        y: 21
        width: 384
        height: 56
        title: qsTr("")

        Label {
            id: arduinoPort
            x: 0
            y: 10
            width: 118
            height: 32
            text: qsTr("Arduino Port :")
        }

        Label {
            id: portCom
            x: 132
            y: 10
            width: 66
            height: 22
            text: qsTr("")
        }

        Button {
            id: testArduino
            x: 245
            y: -8
            text: qsTr("Test arduino")
            onClicked: {

                portCom.text = py_MainApp.testArduino()



            }
        }
    }



}

